﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            this.values = new Dictionary<Tuple<TKey1, TKey2>,TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach (TKey1 key1 in keys1)
            {
                foreach (TKey2 key2 in keys2)
                {
                    this.values.Add(new Tuple<TKey1, TKey2>(key1, key2), generator.Invoke(key1, key2));
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (this.NumberOfElements != other.NumberOfElements)
            {
                return false;
            }
            foreach (Tuple<TKey1, TKey2, TValue> elem in other.GetElements())
            {
                if(this[elem.Item1, elem.Item2] == null || !this[elem.Item1, elem.Item2].Equals(elem.Item3))
                {
                    return false;
                }
            }
            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                return this.values[new Tuple<TKey1, TKey2>(key1, key2)];
            }

            set
            {
                this.values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> key in this.values.Keys)
            {
                if (key.Item1.Equals(key1))
                {
                    list.Add(new Tuple<TKey2, TValue>(key.Item2, this[key1, key.Item2]));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> list = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> key in this.values.Keys)
            {
                if(key.Item2.Equals(key2))
                {
                    list.Add(new Tuple<TKey1, TValue>(key.Item1, this[key.Item1, key2]));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> key in this.values.Keys)
            {
                list.Add(new Tuple<TKey1, TKey2, TValue>(key.Item1, key.Item2, this[key.Item1, key.Item2]));
            }
            return list;
        }

        public int NumberOfElements
        {
            get
            {
                return this.values.Keys.Count;
            }
        }

        public override string ToString()
        {
            string result = "Map2D :\n";
            foreach (Tuple<TKey1, TKey2, TValue> key in this.GetElements())
            {
                result += $"[{key.Item1}][{key.Item2}] -> {key.Item3} \n";
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
