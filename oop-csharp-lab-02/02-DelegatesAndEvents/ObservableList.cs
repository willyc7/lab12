﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {

        private IList<TItem> items;

        public ObservableList()
        {
            this.items = new List<TItem>();
        }
        
        public IEnumerator<TItem> GetEnumerator()
        {
            return this.items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            this.items.Add(item);
            this.ElementInserted?.Invoke(this, item, this.items.IndexOf(item));
        }

        public void Clear()
        {
            this.items = new List<TItem>();
        }

        public bool Contains(TItem item)
        {
            return this.items.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            if (array.Length <= arrayIndex && this.items.Count <= arrayIndex)
            {
                this.items.CopyTo(array, arrayIndex);
            }
        }

        public bool Remove(TItem item)
        {
            int index;
            if ((index = this.items.IndexOf(item)) != -1)
            {
                this.items.Remove(item);
                this.ElementRemoved(this, item, index);
                return true;
            }
            return false;
        }

        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }
        public int IndexOf(TItem item)
        {
            return this.items.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            TItem old = this.items[index];
            this.items.Insert(index, item);
            this.ElementChanged(this, item, old, index);
        }

        public void RemoveAt(int index)
        {
            TItem item = this.items[index];
            this.items.RemoveAt(index);
            this.ElementRemoved(this, item, index);
        }

        public TItem this[int index]
        {
            get { return this.items[index]; }
            set { this.Insert(index, value); }
        }

        public event ListChangeCallback<TItem> ElementInserted ;
        public event ListChangeCallback<TItem> ElementRemoved ;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}