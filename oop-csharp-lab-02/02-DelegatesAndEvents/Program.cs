﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    class Program
    {
        /*
         * == README ==
         * 
         * Il progetto consiste in una libreria che fornisce l'astrazione di "lista osservabile". 
         * 
         * L'intefaccia IObservableList estende IList (interfaccia standard di .Net) con la capacità 
         * della lista di generare eventi ogni volta che subisce una modifica.
         * 
         * L'esercizio fornisce già i delegati ListChangeCallback e ListElementChangeCallback. 
         * Il primo codifica la firma dei metodi che possono essere registrati per intercettare
         * l'aggiunta/rimozione di elementi dalla lista. Il secondo codifica la firma dei metodi che 
         * possono essere registrati per intercettare la sostituzione di un elemento della lista.
         * 
         * Scopo dell'esercizio è implementare la classe ObservableList in maniera tale da generare gli
         * eventi in maniera corretta. 
         * 
         * Il test contenuto nella classe Program tenta di chiarire il comportamento atteso per le
         * implementazioni dell'interfaccia IObservableList. Esso mostra inoltre come è possibile
         * registrare ascoltatori di eventi per una specifica lista.
         */

        static void Main(string[] args)
        {
            var c1 = false;
            var c2 = false;
            var c3 = false;

            IObservableList<int> list = new ObservableList<int>() {1, 2, 3};

            list.ElementInserted += (lst, value, index) => {
                if (lst == list && value == 4 && index == 3)
                {
                    c1 = true;
                }
                else
                {
                    c1 = false;
                }
            };

            list.Add(4);

            list.ElementRemoved += (lst, value, index) => {
                if (lst == list && value == 2 && index == 1)
                {
                    c2 = true;
                }
                else
                {
                    c2 = false;
                }
            };

            list.Remove(2);

            list.ElementChanged += (lst, value, oldValue, index) => {
                if (lst == list && value == 6 && oldValue == 1 && index == 0)
                {
                    c3 = true;
                }
                else
                {
                    c3 = false;
                }
            };

            list[0] = 6;

            if (!(c1 && c2 && c3))
            {
                throw new Exception("Wrong implementation");
            }

            Console.WriteLine("Fine");
        }
    }
}
